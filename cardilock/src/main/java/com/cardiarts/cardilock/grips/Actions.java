package com.cardiarts.cardilock.grips;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.LauncherApps;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Process;
import android.os.UserHandle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.cardiarts.cardilock.frags.EnableAppsFragment.refresh_action;
import static com.cardiarts.cardilock.grips.CL_DBHelper.deleteInactive;
import static com.cardiarts.cardilock.grips.CL_DBHelper.getAllowedPackages;
import static com.cardiarts.cardilock.grips.CL_DBHelper.getIdsAndStatuses;
import static com.cardiarts.cardilock.grips.CL_DBHelper.isComponentAllowed;
import static com.cardiarts.cardilock.grips.CL_DBHelper.storeAlls;
import static com.cardiarts.cardilock.grips.CL_DBHelper.updateStatuses;

/**
 * Created by ben on 12/06/2016.
 */
public class Actions {

    public static LayoutInflater getLayoutInflater(){
        return (LayoutInflater) OActions.refCon.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static void showShortToast(String msg){
        Toast.makeText(OActions.refCon,msg,Toast.LENGTH_SHORT).show();
    }

    public static void showLongToast(String msg){
        Toast.makeText(OActions.refCon,msg,Toast.LENGTH_LONG).show();
    }

    public static List<LauncherActivityInfo> getAllowedLaunchables(){
        List<LauncherActivityInfo> t = getAllAppsLauncher();

        Iterator<LauncherActivityInfo> laii = t.iterator();
        while(laii.hasNext()){
            LauncherActivityInfo n = laii.next();
            ComponentName lcn = n.getComponentName();
            String pn = lcn.getPackageName();
            String cn = lcn.getClassName();
            if(pn == OActions.refCon.getPackageName() || !isComponentAllowed(pn,cn))
                laii.remove();
        }

        return t;
    }


    public static List<LauncherActivityInfo> getAllowedApps(){
        String allowed = "";//OActions.refCon.getPackageName()+" ";
        Cursor allowed_packages = getAllowedPackages();
        if(allowed_packages!= null && allowed_packages.getCount() > 0){
            for(int i = 0; i < allowed_packages.getCount(); i++)
                if(allowed_packages.moveToNext())
                    allowed+=allowed_packages.getString(0)+" ";
        }

        List<LauncherActivityInfo> launchers = getAllAppsLauncher();

        Iterator<LauncherActivityInfo> i = launchers.iterator();
        List<LauncherActivityInfo> tr = new ArrayList<>();
        logDebug("Filteringn all launch pads");
        while(i.hasNext()){
            LauncherActivityInfo cur = i.next();
            String cur_pn = cur.getComponentName().getPackageName();
            logDebug("Filtering launchPads: package of current is: "+cur_pn);
            if(!getAllowAll()) {
                if (allowed.contains(cur_pn))
                    tr.add(cur);
            }else
                tr.add(cur);
        }
        logDebug("Returning allowed apps as: "+tr);
        return tr;
    }

    public static void noteAllLaunchables(){
        logDebug("Noting all launchables");
        Cursor before = getIdsAndStatuses();//get the currents items and their statuses;
        // these contain only id and status
        HashMap<String,String> ids_to_status = new HashMap<>();
        if(before!= null && before.getCount() > 0){
            for(int i = 0; i < before.getCount();i++)
                if(before.moveToNext())
                    ids_to_status.put(before.getString(0),before.getString(1));
        }
        storeAlls(getAllAppsLauncher());// at this point all apps in list are allowed
        deleteInactive();//make the list have only installed apps; note that each time this runs,
        updateStatuses(ids_to_status);//restore allowed or disallowed status
        broadCaseRefresh();
    }

    public static void broadCaseRefresh(){
        LocalBroadcastManager.getInstance(OActions.refCon).sendBroadcast(new Intent(refresh_action));
    }

    public static List<LauncherActivityInfo> getAllAppsLauncher(){
        LauncherApps la = (LauncherApps) OActions.refCon.getSystemService(Context.LAUNCHER_APPS_SERVICE);
        UserHandle uh = Process.myUserHandle();
        return la.getActivityList(null,uh);
    }


    /*
    get a list of all
     */

    public static class Prefkeys{
        public static final String localControlOrRemoteControl = "islocalorremotecontrolled";
        public static final String panel_access_code = "panelpassword";
    }

    public static class PrefsCategories{
        public static final String Settings = "Settings";
        public static final String Values = "Values";
    }

    public static boolean getAllowAll(){
        SharedPreferences sp = OActions.refCon.getSharedPreferences(PrefsCategories.Settings,Context.MODE_PRIVATE);
        return sp.getBoolean(Prefkeys.localControlOrRemoteControl,false);
    }

    public static String getControlPanelPassword(){
        SharedPreferences sp = OActions.refCon.getSharedPreferences(PrefsCategories.Values,Context.MODE_PRIVATE);
        return sp.getString(Prefkeys.panel_access_code,"0000");
    }

    public static boolean setPanelPassword(String new_password){
        SharedPreferences sp = OActions.refCon.getSharedPreferences(PrefsCategories.Values,Context.MODE_PRIVATE);
        return sp.edit().putString(Prefkeys.panel_access_code,new_password).commit();
    }


    public static void logDebug(Object caller,String msg){
        Log.d(OActions.refCon.getPackageName()+"["+caller.getClass().getSimpleName()+"]",msg);
    }

    public static void logDebug(String msg){
        Log.d(OActions.refCon.getPackageName()+"[general]",msg);
    }

    ////////////////////////////////////////F O R     PREFS
    private static boolean storeAVal(String p_n,String v_i,String p_v){
        SharedPreferences sp = OActions.refCon.getSharedPreferences(p_n,Context.MODE_PRIVATE);
        return sp.edit().putString(v_i,p_v).commit();
    }

    private static String getAVal(String p_n,String v_i,String def){
        SharedPreferences sp = OActions.refCon.getSharedPreferences(p_n,Context.MODE_PRIVATE);
        return sp.getString(v_i,def);
    }


    private static boolean storeAVal(String p_n,String v_i,boolean p_v){
        SharedPreferences sp = OActions.refCon.getSharedPreferences(p_n,Context.MODE_PRIVATE);
        return sp.edit().putBoolean(v_i,p_v).commit();
    }

    private static boolean getAVal(String p_n,String v_i,boolean def){
        SharedPreferences sp = OActions.refCon.getSharedPreferences(p_n,Context.MODE_PRIVATE);
        return sp.getBoolean(v_i,def);
    }
    //////////////////////////////E       O        FOR     PREFS


    public static boolean setUnlockTime(long time){
        return storeAVal("vals","unlock_time",time+"");
    }

    public static long getUnlockTime(){
        String ut = getAVal("vals","unlock_time",null);
        if(ut == null)
            return -1000;
        return Long.parseLong(ut);
    }

    public static boolean unlockSys(){
        return storeAVal("vals","unlock_time",-1000+"");
    }

    public static boolean storeFBid(String id){
        return storeAVal("vals","fb_id",id);
    }

    public static String getFBid(){
        return getAVal("vals","fb_id",null);
    }

    public static boolean set_FBt_synced(boolean v){
        return storeAVal("vals","fb_t_s'd",v);
    }

    public static boolean get_FBt_synced(){
        return getAVal("vals","fb_t_s'd",false);
    }

    public static String getDeviceImei(){
        String tr = "";
        TelephonyManager tm = (TelephonyManager) OActions.refCon.getSystemService(Context.TELEPHONY_SERVICE);
        tr = tm.getDeviceId()+"";
        return tr;
    }

    public static String getDeviceNameandModel(){
        String tr = "";
        tr+= Build.MANUFACTURER+":::"+ Build.MODEL;
        return tr;
    }

    public static String getDeviceMA(){
        WifiManager wm = (WifiManager) OActions.refCon.getSystemService(Context.WIFI_SERVICE);
        return wm.getConnectionInfo().getMacAddress();
    }

    public static boolean isConNEcTed(){
        ConnectivityManager cm =
                (ConnectivityManager) OActions.refCon.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    private static String scAc(String sdfd){
        return sdfd;
    }

    public static boolean store_ac(String ac){
        return storeAVal("vals","ac",scAc(ac));
    }

    static{
        if(get_ac().isEmpty())
            store_ac("0000");
    }

    public static String get_ac(){
        return getAVal("vals","ac","");
    }

    public static boolean is_ac(String tc){
        return scAc(tc).equals(get_ac());
    }

    /**
     * @return treemaps of packages to components(full classname) that are launchable
     */
    public static List<ComponentName> getLaunchablePackageComponents(){
        List<ComponentName> tr = new ArrayList<>();
        List<LauncherActivityInfo> li = getAllAppsLauncher();
        Iterator<LauncherActivityInfo> lii = li.iterator();
        while(lii.hasNext()){
            ComponentName cn = lii.next().getComponentName();
            String app_package_name = cn.getPackageName();
            String className = cn.getClassName();
            if(!app_package_name.equals(OActions.refCon.getPackageName()))
                tr.add(cn);
        }
        return tr;
    }

    /**
     * Returns the launchers of applications that isn't this guy*/
    public static List<LauncherActivityInfo> getOtherLaunchers(){
        List<LauncherActivityInfo> li = getAllAppsLauncher();
        Iterator<LauncherActivityInfo> laii = li.iterator();
        while(laii.hasNext()){
            LauncherActivityInfo n = laii.next();
            if(n.getComponentName().getPackageName().equals(OActions.refCon.getPackageName()))
                laii.remove();
        }
        return li;
    }

    public static Map<String,Boolean> launchablepackages_and_isallowed(){
        Set<String> launchable_packages = new HashSet<>();
        List<LauncherActivityInfo> li = getAllAppsLauncher();
        Iterator<LauncherActivityInfo> lii = li.iterator();
        while(lii.hasNext()){
            String app_package_name = lii.next().getComponentName().getPackageName();
            if(!app_package_name.equals(OActions.refCon.getPackageName()))
                launchable_packages.add(app_package_name);
        }
        logDebug("Launchable apps are: "+launchable_packages);

        Cursor c = getAllowedPackages();
        String allowed_launchables = " ";
        int ci = 0;
        if(c != null && (ci = c.getCount()) > 0){
            for(int i = 0; i < ci;i ++)
                if(c.moveToNext())
                    allowed_launchables+= c.getString(0)+" ";
        }
        logDebug("Allowed launchables are: "+allowed_launchables);

        HashMap<String,Boolean> tr = new HashMap<>();
        Iterator<String> si = launchable_packages.iterator();
        while(si.hasNext()){
            String cla = si.next();
            tr.put(cla,allowed_launchables.contains(cla));
        }

        logDebug("Launchables and statuses to return are: \n"+tr);
        return tr;
    }

}
