package com.cardiarts.cardilock.grips;

/**
 * Holds components of a package that can be launched
 * Created by ben on 07/10/2016.
 */

public class LaunchableComponent {
    private String packageName;
    private String componentClassName;

    public LaunchableComponent(String componentClassName, String packageName) {
        this.componentClassName = componentClassName;
        this.packageName = packageName;
    }
}
