package com.cardiarts.cardilock.grips;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cardiarts.cardilock.acts.Shortcuts;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.cardiarts.cardilock.frags.EnableSettingsFragment.default_allowed_settings;
import static com.cardiarts.cardilock.grips.Actions.getAllAppsLauncher;
import static com.cardiarts.cardilock.grips.Actions.logDebug;

/**
 * Created by ben on 12/06/2016.
 */
public class CL_DBHelper extends SQLiteOpenHelper {


    private static final String Database_Name = "CardiOwner Database";
    private static final int DATABASE_VERSION = 1;

    private static SQLiteDatabase mydb;
    public static CL_DBHelper instance = null;
    static Context ct;

    public CL_DBHelper(Context context) {
        super(context, Database_Name, null, DATABASE_VERSION);
        mydb = getWritableDatabase();
        ct = context;
        instance = this;
    }

    public static String getDBName()
    {
        return Database_Name;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        mydb = db;
        db.execSQL(create_launching_pad_apps_table);
        db.execSQL(create_settings_table);
        inflate_default_settings_access(db);
        initialNoteAllLaunchables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion == 1 && newVersion == 2) {

        }
    }

    private static final String launching_pads = "laucher_activities";
    private static final String lauching_pads_package_name  = "package_name";
    private static final String launching_pads_labels = "labels";
    private static final String lauching_pads_app_version = "version";
    private static final String lauching_pads_app_size = "size";
    private static final String lauching_pads_component_classname = "comp_name";
    private static final String lauching_pads_component_active = "is_active";
    private static final String launching_pad_app_allowed_status = "is_allowed";
    private static final String lauching_pads_comp_id = "com_id";

    public static Cursor getAllowedLaunchersCompoenents(){
        return mydb.query(launching_pads,new String[]{lauching_pads_component_classname},
                launching_pad_app_allowed_status+" = ? ",new String[]{"1"},null,null,null);
    }

    private static final String create_launching_pad_apps_table =
            "CREATE TABLE "+ launching_pads +" ("
                    + lauching_pads_package_name +"  TEXT ,"
                    + lauching_pads_comp_id +"  TEXT not null,"
                    +launching_pads_labels+"  TEXT NOT NULL,"
                    +lauching_pads_app_version+"  TEXT(10) NOT NULL,"
                    //+lauching_pads_app_size+"  TEXT NOT NULL,"
                    +launching_pad_app_allowed_status+" TEXT(5) DEFAULT '1',"
                    + lauching_pads_component_classname +" TEXT ,"
                    +lauching_pads_component_active+" TEXT DEFAULT '1',"
                    +" PRIMARY KEY("+lauching_pads_comp_id+")" +
                    ");";


    public static Cursor getAllowedPackages(){
        return mydb.query(launching_pads,new String[]{lauching_pads_package_name},
                launching_pad_app_allowed_status+" = ?",new String[]{"1"},null,null,null);
    }

    public static Cursor getAllowedLaunchables(){
        return mydb.query(launching_pads,new String[]{lauching_pads_package_name,lauching_pads_component_classname},
                launching_pad_app_allowed_status+" = ?",new String[]{"1"},null,null,null);
    }

    public static int storeAlls(List<LauncherActivityInfo> a)
    {
        ContentValues cv = new ContentValues();
        cv.put(lauching_pads_component_active,false);
        mydb.update(launching_pads,cv,null,null);
        int successfullyadded = 0;

        Iterator<LauncherActivityInfo> i = a.iterator();
        while(i.hasNext()){
            LauncherActivityInfo io = i.next();
            String pn = io.getApplicationInfo().packageName;
            if(pn.equals(OActions.refCon.getPackageName()))
                continue;

            String componentClass = io.getComponentName().getClassName();
            //getClassName returns the qualified classname of the component

            String launcher_act_label = (String) io.getLabel();
            String app_version = "version not found";
            String app_size ;
            try {
                PackageManager pm = OActions.refCon.getPackageManager();
                PackageInfo pi = pm.getPackageInfo(pn,0);
                app_version = pi.versionCode+"";
                if(addAppLauncher(pn,componentClass,app_version,launcher_act_label))
                    successfullyadded++;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        logDebug("Call to store all apps in table done; inserted "+successfullyadded+" launchers");
        return successfullyadded;
    }


    public static void initialNoteAllLaunchables(SQLiteDatabase db){
        logDebug("Initial reg of launchables ongoing...");
//        Cursor before = getIdsAndStatuses();//get the currents items and their statuses;
//        // these contain only id and status
//        HashMap<String,String> ids_to_status = new HashMap<>();
//        if(before!= null && before.getCount() > 0){
//            for(int i = 0; i < before.getCount();i++)
//                if(before.moveToNext())
//                    ids_to_status.put(before.getString(0),before.getString(1));
//        }
        regLaunchables(db,getAllAppsLauncher());// at this point all apps in list are allowed
//        deleteInactive();//make the list have only installed apps; note that each time this runs,
//        updateStatuses(ids_to_status);//restore allowed or disallowed status
//        broadCaseRefresh();
    }

    public static int regLaunchables(SQLiteDatabase mydb,List<LauncherActivityInfo> a)
    {
        ContentValues cv = new ContentValues();
        cv.put(lauching_pads_component_active,false);
        mydb.update(launching_pads,cv,null,null);
        int successfullyadded = 0;

        Iterator<LauncherActivityInfo> i = a.iterator();
        while(i.hasNext()){
            LauncherActivityInfo io = i.next();
            String pn = io.getApplicationInfo().packageName;
            if(pn.equals(OActions.refCon.getPackageName()))
                continue;

            String componentClass = io.getComponentName().getClassName();
            //getClassName returns the qualified classname of the component

            String launcher_act_label = (String) io.getLabel();
            String app_version = "version not found";
            String app_size ;
            try {
                PackageManager pm = OActions.refCon.getPackageManager();
                PackageInfo pi = pm.getPackageInfo(pn,0);
                app_version = pi.versionCode+"";
                if(addAppLauncher(pn,componentClass,app_version,launcher_act_label))
                    successfullyadded++;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        logDebug("Call to store all apps in table done; inserted "+successfullyadded+" launchers");
        return successfullyadded;
    }

    public static void deleteInactive(){
        mydb.delete(launching_pads,lauching_pads_component_active+" = ?", new String[]{"0"});
    }

    private static boolean addAppLauncher(String packageName,String componentClassName,String app_version,String launcher_act_label){
        ContentValues cv = new ContentValues();
        cv.put(lauching_pads_comp_id,packageName+":"+componentClassName);
        cv.put(lauching_pads_package_name,packageName);
        cv.put(launching_pads_labels,launcher_act_label);
        cv.put(lauching_pads_app_version,app_version);
        cv.put(lauching_pads_component_classname,componentClassName);
        long rioe = mydb.insertWithOnConflict(launching_pads,null,cv,SQLiteDatabase.CONFLICT_REPLACE);
        boolean result;
        logDebug("Rice is: "+rioe+" result is: "+(result = (rioe != -1)));
        return result;
    }

    public static int changeAppAllowedStatus(String pn,String componentClassname,boolean is_allowed){
        ContentValues cv = new ContentValues();
        cv.put(launching_pad_app_allowed_status,is_allowed);
        return mydb.update(launching_pads,cv,lauching_pads_package_name+" = ? " +
                "AND "+lauching_pads_component_classname+" = ?",new String[]{pn,componentClassname});
    }

    public static int setStatusForAllLaunchableComponents(boolean is_allowed){
        ContentValues cv = new ContentValues();
        cv.put(launching_pad_app_allowed_status,is_allowed);
        return mydb.update(launching_pads,cv,null,null);
    }

    public static int setStatusForAllLaunchableSettings(boolean is_allowed){
        ContentValues cv = new ContentValues();
        cv.put(setting_allowed,is_allowed);
        return mydb.updateWithOnConflict(setting_list,cv,null,null,SQLiteDatabase.CONFLICT_IGNORE);
    }


    public static boolean isComponentAllowed(String packageName,String component_classname){
        Cursor r = mydb.query(launching_pads,new String[]{launching_pad_app_allowed_status},
                lauching_pads_package_name+" = ? AND "+lauching_pads_component_classname+" = ?",
                new String[]{packageName,component_classname},null,null,null);
        if(r == null || r.getCount() < 1)
            return false;

        return r.moveToNext() && r.getString(0).equals("1");
    }

    public static Cursor getIdsAndStatuses(){
        return mydb.query(launching_pads,new String[]{lauching_pads_comp_id,launching_pad_app_allowed_status},null,null,null,null,null);
    }

    public static int updateStatuses(HashMap<String,String> old_statuses){
        int update_count = 0 ;
        Iterator<Map.Entry<String,String>> is = old_statuses.entrySet().iterator();
        while(is.hasNext()){
            Map.Entry<String,String> n_comp = is.next();
            if(updateComponentStatusByID(n_comp.getKey(),n_comp.getValue()))
                update_count++;
        }
        logDebug("RestoredStatuses for "+update_count+" components");
        return update_count;
    }

    private static boolean updateComponentStatusByID(String component_id,String status){
        ContentValues cv = new ContentValues();
        cv.put(launching_pad_app_allowed_status,status);
        return mydb.update(launching_pads,cv,lauching_pads_comp_id+" = ?", new String[]{component_id}) >= 0;
    }

















    private static final String setting_name = "name";
    private static final String setting_int  = "action";
    private static final String setting_allowed = "is_allowed";
    private static final String setting_list = "Settings";

    private static final String create_settings_table =
            "CREATE TABLE "+ setting_list +" ("
                    + setting_name +"  TEXT NOT NULL,"
                    +setting_allowed+"  TEXT NOT NULL default '1',"
                    +setting_int+" TEXT NOT NULL,"
                    +" PRIMARY KEY("+setting_int+"));";

    private static void inflate_default_settings_access(SQLiteDatabase db){
        List<Shortcuts.A_setting_tag> ta = default_allowed_settings();

        Iterator<Shortcuts.A_setting_tag> si = ta.iterator();
        ContentValues cv = new ContentValues();
        int total_added = 0;
        while(si.hasNext()){
            Shortcuts.A_setting_tag cur = si.next();
            cv.put(setting_name,cur.getName());
            cv.put(setting_allowed,cur.isAllowed());
            cv.put(setting_int,cur.getAct());

            if(db.insertWithOnConflict(setting_list,null,cv,SQLiteDatabase.CONFLICT_REPLACE) != -1)
                total_added++;
        }
        logDebug("inserted "+total_added+" accessible Settings");
    }

    public static Cursor getShortcuts(){
        return mydb.query(setting_list,null,null,null,null,null,null);
    }

    public static Cursor getAccessibleSystemSettings(){
        return mydb.query(setting_list,null,setting_allowed+" = ? ",new String[]{"1"},null,null,null);
    }

    public static boolean change_setting_accessible_state(String setting_act, boolean is){
        ContentValues cv = new ContentValues();
        cv.put(setting_allowed,is);
        return mydb.updateWithOnConflict(setting_list,cv,setting_int+" = ?",new String[]{setting_act},SQLiteDatabase.CONFLICT_IGNORE) > 0;
    }
}
