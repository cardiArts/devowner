package com.cardiarts.cardilock.grips;

import android.app.Activity;
import android.content.ComponentName;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.cardiarts.cardilock.R;

import java.util.List;

import static com.cardiarts.cardilock.grips.Actions.getLayoutInflater;
import static com.cardiarts.cardilock.grips.Actions.getOtherLaunchers;
import static com.cardiarts.cardilock.grips.Actions.logDebug;
import static com.cardiarts.cardilock.grips.CL_DBHelper.changeAppAllowedStatus;
import static com.cardiarts.cardilock.grips.CL_DBHelper.isComponentAllowed;

/**
 * Created by ben on 12/06/2016.
 */
public class AllAppsSatusEditAdapter extends BaseAdapter{

    private final Activity act;
    private final List<LauncherActivityInfo> launchables;


    public AllAppsSatusEditAdapter(Activity activity) {
        this.act = activity;
        this.launchables = getOtherLaunchers();
        //Toast.makeText(activity,"Retrieved "+this.launchables.size()+" launchables", Toast.LENGTH_LONG).show();
    }


    @Override
    public int getCount() {
        return launchables.size();
    }

    @Override
    public LauncherActivityInfo getItem(int position) {
        return launchables.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View tr = getLayoutInflater().inflate(R.layout.edit_allowed_apps_list_item,parent,false);
        TextView l = (TextView) tr.findViewById(R.id.et_app_name); l.setTextColor(Color.BLACK);
        ImageView ai = (ImageView) tr.findViewById(R.id.app_icon);
        TextView sn = (TextView) tr.findViewById(R.id.textView2);sn.setTextColor(Color.BLACK);
        final Switch cb = (Switch) tr.findViewById(R.id.cb_is_allowed);
        sn.setText((position+1)+"");

        LauncherActivityInfo curLau = getItem(position);
        ComponentName cur = curLau.getComponentName();
        String which_package = cur.getPackageName();
        String componenetClassName = cur.getClassName();

        cb.setTag(which_package+"::"+componenetClassName);
        cb.setChecked(isComponentAllowed(which_package,componenetClassName));//appsNstats.get(which_package));
        try {
            PackageManager pm = OActions.refCon.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(which_package,0);
            l.setText(curLau.getLabel());
            ai.setImageDrawable(curLau.getIcon(0));
            //l.setText(""+pm.getApplicationLabel(pi.applicationInfo));
            //ai.setImageDrawable(pi.applicationInfo.loadIcon(pm));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String[] ti = ((String) buttonView.getTag()).split("::");
                //the above gets the {package_name,component_class_name}
                logDebug("changing status of ["+ti[0]+"] to "+(isChecked?"Allowed":"Disallowed"));
                int ce = changeAppAllowedStatus(ti[0],ti[1],isChecked);
                logDebug("Changed effected in "+ce+" rows");
            }
        });
        tr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logDebug(" something has been clicked");
                cb.setChecked(!cb.isChecked());
            }
        });
        return tr;
    }
}
