package com.cardiarts.cardilock.grips;

import android.content.Context;
import android.view.MotionEvent;
import android.view.ViewGroup;

/**
 * Created by ben on 12/06/2016.
 */
public class customViewGroup extends ViewGroup{
    public customViewGroup(Context myAct) {
        super(myAct);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return true;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

    }
}
