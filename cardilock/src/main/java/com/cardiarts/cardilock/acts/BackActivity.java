package com.cardiarts.cardilock.acts;

import android.content.Intent;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.LauncherApps;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Process;
import android.os.UserHandle;
import android.provider.Settings;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cardiarts.cardilock.grips.OActions;
import com.cardiarts.cardilock.R;
import com.cardiarts.cardilock.grips.StatusBarService;
import com.cardiarts.cardilock.grips.customViewGroup;
//import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static com.cardiarts.cardilock.grips.Actions.getFBid;
import static com.cardiarts.cardilock.grips.Actions.logDebug;
import static com.cardiarts.cardilock.grips.Actions.noteAllLaunchables;
import static com.cardiarts.cardilock.grips.Actions.storeFBid;
import static com.cardiarts.cardilock.grips.OActions.init;

public class BackActivity extends MyBaseActivity {

    private View tp;
    private customViewGroup view;
    private boolean hv;
    private TextView tv;
    private EditText action;
    private EditText em;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(this);
        setContentView(R.layout.activity_my_testground);
        tv = (TextView) findViewById(R.id.tv);
        action = (EditText) findViewById(R.id.et_int_action);
        em = (EditText) findViewById(R.id.cust_err_msg);
    }

    public void onStart(){
        super.onStart();

        Bundle b = getIntent().getExtras();
        if(b == null){
            logDebug(this,"intent that started me has no extras");
        }else{
            Set<String> keys = b.keySet();
            Iterator<String> it = keys.iterator();
            logDebug(this,"Dumping Intent start");
            while (it.hasNext()) {
                String key = it.next();
                logDebug(this,"[" + key + "=" + b.get(key)+"]");
            }
            logDebug(this,"Dumping Intent end");
        }

        LauncherApps la = (LauncherApps) getSystemService(LAUNCHER_APPS_SERVICE);
        UserHandle uh = Process.myUserHandle();
        List<LauncherActivityInfo> launchers = la.getActivityList(null,uh);
        Iterator<LauncherActivityInfo> i = launchers.iterator();
        while(i.hasNext()){
            tv.setText(tv.getText()+"\n"+
                    i.next().getLabel());
        }
    }

    public void onClick(View v){
        int vid = v.getId();

        if(vid == R.id.btn_na)
            noteAllLaunchables();

        if(vid == R.id.btn_do_creasherror){
            String err_m = em.getText().toString();
            if(err_m.isEmpty()){
                Toast.makeText(this, "Specify custom error message action", Toast.LENGTH_SHORT).show();
                return;
            }

            //FirebaseCrash.report(new Exception(err_m));
        }

        if(vid ==  R.id.btn_rest)
            startActivity(new Intent(this,Restrictions.class));

        if(vid == R.id.btn_fb_id){
            String fbid = getFBid();
            Toast.makeText(this,"FB ID: "+fbid,Toast.LENGTH_LONG).show();
        }

        if(vid == R.id.btn_g_fbid){
            String id = FirebaseInstanceId.getInstance().getToken();
            logDebug(this,"Fb ID gotten as: ["+id+"]");
            storeFBid(id);
        }

        if(vid == R.id.btn_fire){
            String ac = action.getText().toString();
            if(ac.isEmpty()){
                Toast.makeText(this, "Specify action", Toast.LENGTH_SHORT).show();
                return;
            }

            Intent iitf = new Intent(ac);
            PackageManager pm = getPackageManager();
            ResolveInfo ri =
            pm.resolveActivity(iitf,PackageManager.MATCH_DEFAULT_ONLY);

            if(ri == null)
                Toast.makeText(this,"Not found!!!",Toast.LENGTH_SHORT).show();
            else
                startActivity(iitf);
        }

        if(vid == R.id.btn_wifi_set)
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));

        if(vid == R.id.btn_dev_owner)
            startActivity(new Intent(this,MainActivity.class));

        if(vid == R.id.btn_bsb)
            startService(new Intent(this,StatusBarService.class));

        if(vid == R.id.stopStoppingstatusbar)
            stopService(new Intent(this,StatusBarService.class));



        if(vid == R.id.btn_ex)
            OActions.collapseStatusBar();

        if(vid == R.id.btn_col)
            OActions.expandStatusBar();

        if(vid == R.id.btn_l)
            startActivity(new Intent(this,CardiLauncher.class));


    }


}
