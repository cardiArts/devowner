package com.cardiarts.cardilock.acts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.GridView;
import android.widget.ImageView;

import com.cardiarts.cardilock.R;
import com.cardiarts.cardilock.grips.AllowedAppsAdapter;
import com.cardiarts.cardilock.grips.StatusBarService;

import static com.cardiarts.cardilock.frags.EnableAppsFragment.refresh_action;
import static com.cardiarts.cardilock.grips.Actions.getAllowedLaunchables;
import static com.cardiarts.cardilock.grips.Actions.getUnlockTime;
import static com.cardiarts.cardilock.grips.Actions.logDebug;
import static com.cardiarts.cardilock.grips.Actions.showShortToast;
import static com.cardiarts.cardilock.grips.Actions.unlockSys;
import static com.cardiarts.cardilock.grips.OActions.init;

public class CardiLauncher extends MyBaseActivity implements View.OnClickListener {

    GridView apps;
    private AllowedAppsAdapter aa;
    private String apps_list_y_scroll = "sd";
    private FloatingActionButton fab;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(this);
        setContentView(R.layout.activity_cardi_launcher);
        apps = (GridView) findViewById(R.id.gv_apps);
        fab = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(this);
        startService(new Intent(this,StatusBarService.class));
        LocalBroadcastManager.getInstance(this).registerReceiver(mbr,new IntentFilter(refresh_action));
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        setupOptions();
                    }
                }, 1l);

    }

    public void onStart(){
        super.onStart();
        refreshList();
    }

    private void refreshList(){
        aa = new AllowedAppsAdapter(this,getAllowedLaunchables());
        apps.setAdapter(aa);
    }


    private BroadcastReceiver mbr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Toast.makeText(context,"Launcher: refreshing",Toast.LENGTH_LONG).show();
            logDebug(this,"Launcher: refreshing");
            refreshList();
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        retrachOptions();
        //finish();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null)
            apps.smoothScrollToPosition(savedInstanceState.getInt(apps_list_y_scroll));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(apps_list_y_scroll,apps.getScrollY());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        //when fab is clicked, show the options to
        if (v == fab) {
            options_shown = !options_shown;
            if (options_shown)
                showOptions();
            else
                retrachOptions();
        }


        if(v == options_shortcuts || v == options_settings){
            long ut = getUnlockTime();

            if(!(ut < 0 || System.currentTimeMillis() > ut)) {
                showShortToast("System is currently locked");
                retrachOptions();
                return;
            }else
                unlockSys();

            if (v == options_shortcuts)
                startActivity(new Intent(this, Shortcuts.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

            if (v == options_settings)
                startActivity(new Intent(this, CardiLauncherSettings.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

        }
    }

    private void retrachOptions(){
        ViewPropertyAnimator a1 = options_shortcuts.animate().y(-shortcutsInitialY)
                .alpha(0).setDuration(options_settings_animation_duration);
        ViewPropertyAnimator a2 = options_settings.animate().y(-settingsInitialY)
                .alpha(0).setDuration(options_shortcuts_animation_duration);
        a1.start();a2.start();
    }

    long options_animation_duration = 300l;
    long options_settings_animation_duration = options_animation_duration + 100l;
    long options_shortcuts_animation_duration = options_animation_duration;

    private void showOptions(){
        ViewPropertyAnimator a1 = options_shortcuts.animate().translationY(shortcutsInitialY)
                .alpha(1).setDuration(options_settings_animation_duration);
        ViewPropertyAnimator a2 = options_settings.animate().translationY(settingsInitialY)
                .alpha(1).setDuration(options_shortcuts_animation_duration);
        a1.start();a2.start();
    }

    private void setupOptions(){
        options_shortcuts = (ImageView) findViewById(R.id.imageView2);
        options_settings = (ImageView) findViewById(R.id.imageView);

        options_settings.setOnClickListener(this);
        options_shortcuts.setOnClickListener(this);

        settingsInitialY = options_settings.getTranslationY();
        shortcutsInitialY = options_shortcuts.getTranslationY();

        /*options_settings.setTranslationY(-settingsInitialY);
        options_shortcuts.setTranslationY(-shortcutsInitialY);*/
        options_settings.setAlpha(0f);
        options_shortcuts.setAlpha(0f);
        String msg = "Initial settings Y: "+settingsInitialY+" initial shortcuts Y: "+shortcutsInitialY;
        retrachOptions();
        logDebug(this,msg);
        //Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    boolean options_shown = false;

    private ImageView options_shortcuts;
    private ImageView options_settings;
    float settingsInitialY = 0;
    float shortcutsInitialY = 0;

}
