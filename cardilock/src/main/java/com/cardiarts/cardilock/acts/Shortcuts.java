package com.cardiarts.cardilock.acts;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cardiarts.cardilock.R;

import static com.cardiarts.cardilock.grips.CL_DBHelper.getAccessibleSystemSettings;
import static com.cardiarts.cardilock.grips.OActions.init;

public class Shortcuts extends MyBaseActivity{

    Cursor allow_settings;
    private ListView mylv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(this);
        setContentView(R.layout.activity_shortcuts);
        mylv = (ListView) findViewById(R.id.shortcuts_list);
        allow_settings = getAccessibleSystemSettings();
        mylv.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return allow_settings.getCount();
            }

            @Override
            public Cursor getItem(int position) {
                allow_settings.moveToPosition(position);
                return allow_settings;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tr = new TextView(Shortcuts.this);
                Cursor cur = getItem(position);
                String setting = cur.getString(0);
                String setting_action = cur.getString(2);
                tr.setText(setting);/*
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams();
                lp.width = lp.MATCH_PARENT;
                lp.height = lp.WRAP_CONTENT;*/
                tr.setTextSize(45);
                tr.setTag(setting_action);
                tr.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                tr.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent tf = new Intent((String) v.getTag());
                                ResolveInfo ri =  Shortcuts.this.getPackageManager().resolveActivity(tf, PackageManager.MATCH_DEFAULT_ONLY);
                                if(ri == null)
                                    Toast.makeText(Shortcuts.this,"Setting not available on this device",Toast.LENGTH_SHORT).show();
                                else {
                                    //Toast.makeText(Shortcuts.this,"Should fire:> "+cur.getString(2),Toast.LENGTH_SHORT).show();
                                    startActivity(tf);
                                }
                            }
                        }
                );
                return tr;
            }
        });
    }

    public static class A_setting_tag{
        private String name;//  as will be displayed on the interface
        private String act; // the intent action which would lead to the setting page
        boolean allowed = false;

        public A_setting_tag(String act, boolean allowed, String name) {
            this.act = act;
            this.allowed = allowed;
            this.name = name;
        }

        public String getAct() {
            return act;
        }

        public boolean isAllowed() {
            return allowed;
        }

        public String getName() {
            return name;
        }
    }
}
