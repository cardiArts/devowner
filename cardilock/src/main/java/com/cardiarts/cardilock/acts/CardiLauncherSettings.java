package com.cardiarts.cardilock.acts;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cardiarts.cardilock.R;
import com.cardiarts.cardilock.frags.EnableAppsFragment;
import com.cardiarts.cardilock.frags.EnableSettingsFragment;
import com.cardiarts.cardilock.grips.StatusBarService;

import java.util.Calendar;

import static com.cardiarts.cardilock.grips.Actions.broadCaseRefresh;
import static com.cardiarts.cardilock.grips.Actions.getControlPanelPassword;
import static com.cardiarts.cardilock.grips.Actions.setPanelPassword;
import static com.cardiarts.cardilock.grips.Actions.setUnlockTime;
import static com.cardiarts.cardilock.grips.Actions.showLongToast;
import static com.cardiarts.cardilock.grips.CL_DBHelper.setStatusForAllLaunchableComponents;
import static com.cardiarts.cardilock.grips.CL_DBHelper.setStatusForAllLaunchableSettings;
import static com.cardiarts.cardilock.grips.OActions.init;

public class CardiLauncherSettings extends AppCompatActivity implements TabLayout.OnTabSelectedListener {
    private TabLayout tabs;
    private ViewPager mvp;

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(this);
        setContentView(R.layout.launcher_settings);
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        tb.setTitle(getTitle());
        setSupportActionBar(tb);
        tabs = (TabLayout) findViewById(R.id.tabs);
        mvp = (ViewPager) findViewById(R.id.container);
        createFrags();
        mvp.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
        tabs.setOnTabSelectedListener(this);
        tabs.setupWithViewPager(mvp);
        new PanelGate().show(getFragmentManager(),"sdjksk");
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mvp.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        mvp.setCurrentItem(tab.getPosition());
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.panel_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int smiid = item.getItemId();
        if(smiid == R.id.action_backpage)
            startActivity(new Intent(this,BackActivity.class));

        if(smiid == R.id.action_enable_sb)
            stopService(new Intent(this,StatusBarService.class));

        if(smiid == R.id.action_disable_sb)
            startService(new Intent(this,StatusBarService.class));
		
		if(smiid == R.id.action_change_password)
			new ChangePanelGatePass().show(getFragmentManager(),"sdjkfsk");



        if(smiid == R.id.enable_all){
            if(mvp.getCurrentItem() == 0)
                setStatusForAllLaunchableComponents(true);
            else if(mvp.getCurrentItem() == 1)
                setStatusForAllLaunchableSettings(true);
            broadCaseRefresh();
        }

        if(smiid == R.id.disable_all){
            if(mvp.getCurrentItem() == 0)
                setStatusForAllLaunchableComponents(false);
            else if(mvp.getCurrentItem() == 1)
                setStatusForAllLaunchableSettings(false);
            broadCaseRefresh();
        }
        return true;
    }



    private void createFrags() {
        frags = new Fragment[2];
        frags[0] = new EnableAppsFragment();
        frags[1] = new EnableSettingsFragment();
        tabs.addTab(tabs.newTab().setText("Apps"));
        tabs.addTab(tabs.newTab().setText("Shortcuts"));
    }



    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return frags[position];
        }

        @Override
        public int getCount() {
            return frags.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0)
                return "Applications";

            if(position == 1)
                return "Shortcuts";

            return "Unknown";
        }
    }

    private Fragment[] frags;




    @Override
    public void onBackPressed() {
        finish();
    }

    public static class PanelGate extends DialogFragment implements View.OnClickListener, TextView.OnEditorActionListener {
        private EditText cpp;
        private int faild_count = 0;
        private int fcl = 3;
        private View btn_quit;


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog tr = super.onCreateDialog(savedInstanceState);
            tr.setTitle("Input password");
            tr.setCanceledOnTouchOutside(false);
            return tr;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View tr = inflater.inflate(R.layout.login_dialog, null);
            cpp = (EditText) tr.findViewById(R.id.et_password);
            cpp.setOnEditorActionListener(this);
            btn_quit = tr.findViewById(R.id.quit);
            if(btn_quit != null)
                btn_quit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getActivity().finish();
                        dismiss();
                    }
                });
            tr.findViewById(R.id.btn_go).setOnClickListener(this);
            setCancelable(false);
            return tr;
        }

        @Override
        public void onClick(View v) {
            String ep = cpp.getText().toString();
            if(ep.isEmpty()){
                Toast.makeText(getActivity(),"Input a password then try again",Toast.LENGTH_SHORT).show();
                return;
            }

            if(getControlPanelPassword().equals(ep)) {
                dismiss();
                return;
            }

            faild_count++;
            Toast.makeText(getActivity(),"Invalid password entered",Toast.LENGTH_SHORT).show();
            if(faild_count >= fcl) {
                setUnlockTime(getUnlockkableTime());
                getActivity().finish();
                showLongToast("System LOCKED ! !! !!!");
            }
        }

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if(actionId == EditorInfo.IME_ACTION_DONE)
                onClick(btn_quit);
            return true;
        }

        private long getUnlockkableTime(){
            Calendar now = Calendar.getInstance();
            now.add(Calendar.MINUTE,35);
            //now.add(Calendar.SECOND,30);
            return now.getTimeInMillis();
        }
    }
	
    public static class ChangePanelGatePass extends DialogFragment implements View.OnClickListener, TextView.OnEditorActionListener {
        private EditText cpp;
        private EditText cpp2;
        private View btn_quit;


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog tr = super.onCreateDialog(savedInstanceState);
            tr.setTitle("Change password");
            return tr;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View tr = inflater.inflate(R.layout.change_login, null);
            cpp = (EditText) tr.findViewById(R.id.et_password1);
            cpp2 = (EditText) tr.findViewById(R.id.et_password2);
            cpp2.setOnEditorActionListener(this);
            btn_quit = tr.findViewById(R.id.quit);
            if(btn_quit != null)
                btn_quit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getActivity().finish();
                        dismiss();
                    }
                });
            tr.findViewById(R.id.btn_go).setOnClickListener(this);
            return tr;
        }

        @Override
        public void onClick(View v) {
            String ep = cpp.getText().toString();
            String ep2 = cpp2.getText().toString();
			
			
            if(!ep.equals(ep2)){
                Toast.makeText(getActivity(),"Provided passwords not the same",Toast.LENGTH_SHORT).show();
                return;
            }
			
            if(ep.isEmpty()){
                Toast.makeText(getActivity(),"Password cannot be empty",Toast.LENGTH_SHORT).show();
                return;
            }
			
			if(ep.length() < 4){
                Toast.makeText(getActivity(),"Password must be at least 4 characters",Toast.LENGTH_SHORT).show();
                return;
            }

            if(getControlPanelPassword().equals(ep)) {
                Toast.makeText(getActivity(),"New password cannot be the same as old password",Toast.LENGTH_SHORT).show();
                return;
            }
			
			setPanelPassword(ep);
			dismiss();
			Toast.makeText(getActivity(),"Password changed",Toast.LENGTH_SHORT).show();
			return;
        }

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if(actionId == EditorInfo.IME_ACTION_DONE)
                onClick(btn_quit);
            return true;
        }
    }

}
