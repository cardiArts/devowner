package com.cardiarts.cardilock.acts;

import android.os.UserManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;

import com.cardiarts.cardilock.R;

public class Restrictions extends MyBaseActivity {

    private ListView lv_restrictions;
    private UserManager um;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        um = (UserManager) getSystemService(USER_SERVICE);
        setContentView(R.layout.activity_restrictions);
        lv_restrictions = (ListView) findViewById(R.id.lv_restrictions);

        lv_restrictions.setAdapter(
                new BaseAdapter() {
                    @Override
                    public int getCount() {
                        return targeted_restriciont.length;
                    }

                    @Override
                    public String getItem(int position) {
                        return targeted_restriciont[position];
                    }

                    @Override
                    public long getItemId(int position) {
                        return position;
                    }

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        Switch tr = new Switch(Restrictions.this);
                        final String cr = getItem(position);
                        tr.setText(cr);
                        tr.setEnabled(um.getUserRestrictions().containsKey(cr));
                        tr.setOnCheckedChangeListener(
                                new CompoundButton.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                        /*
                                        if(isChecked)
                                            um.addUserRestriction(get_dev_admin_comp(),cr);
                                        else
                                            um.clearUserRestriction(get_dev_admin_comp(),cr);*/
                                        um.setUserRestriction(cr,isChecked);
                                    }
                                }
                        );
                        return null;
                    }
                }
        );
    }

    static String[] targeted_restriciont =
            {UserManager.DISALLOW_ADD_USER,
                    UserManager.DISALLOW_CONFIG_BLUETOOTH,
                    UserManager.DISALLOW_CONFIG_MOBILE_NETWORKS,
                    UserManager.DISALLOW_CONFIG_TETHERING,
                    UserManager.DISALLOW_CONFIG_VPN,
                    UserManager.DISALLOW_CONFIG_WIFI,
                    UserManager.DISALLOW_DEBUGGING_FEATURES,
                    UserManager.DISALLOW_FACTORY_RESET,
                    UserManager.DISALLOW_FUN,
                    UserManager.DISALLOW_INSTALL_APPS,
                    UserManager.DISALLOW_MODIFY_ACCOUNTS,
                    UserManager.DISALLOW_MOUNT_PHYSICAL_MEDIA,
                    UserManager.DISALLOW_NETWORK_RESET,
                    UserManager.DISALLOW_OUTGOING_CALLS,
                    UserManager.DISALLOW_REMOVE_USER,
                    UserManager.DISALLOW_REMOVE_USER,
                    UserManager.DISALLOW_SAFE_BOOT,
                    UserManager.DISALLOW_SMS,
                    UserManager.DISALLOW_UNINSTALL_APPS,
                    UserManager.DISALLOW_USB_FILE_TRANSFER};
}
