package com.cardiarts.cardilock.frags;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.cardiarts.cardilock.R;
import com.cardiarts.cardilock.acts.Shortcuts;

import java.util.ArrayList;
import java.util.List;

import static com.cardiarts.cardilock.frags.EnableAppsFragment.refresh_action;
import static com.cardiarts.cardilock.grips.Actions.logDebug;
import static com.cardiarts.cardilock.grips.CL_DBHelper.change_setting_accessible_state;
import static com.cardiarts.cardilock.grips.CL_DBHelper.getShortcuts;

/**
 * Created by Benjamin Awesome on 8/8/2016.
 */
public class EnableSettingsFragment  extends Fragment {

    private Cursor all_shortcuts;
    private ListView aalv;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        all_shortcuts = getShortcuts();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mbr,new IntentFilter(refresh_action));
    }

    BaseAdapter shortcuts_list_adapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return all_shortcuts.getCount();
        }

        @Override
        public Cursor getItem(int position) {
            all_shortcuts.moveToPosition(position);
            return all_shortcuts;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View tr = getActivity().getLayoutInflater().inflate(R.layout.edit_shortcut,null);
            TextView n = (TextView) tr.findViewById(R.id.et_short_cut_name);
            final CheckBox scb = (CheckBox) tr.findViewById(R.id.cb_is_shortcut_allowed);
            Cursor cs = getItem(position);
            tr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    scb.setChecked(!scb.isChecked());
                }
            });
            n.setText(cs.getString(0));
            scb.setChecked(cs.getString(1).equals("1"));
            scb.setTag(cs.getString(2));
            scb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    change_setting_accessible_state((String) buttonView.getTag(),isChecked);
                }
            });
            return tr;
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View tr = inflater.inflate(R.layout.activity_shortcuts,null);
        aalv = (ListView) tr.findViewById(R.id.shortcuts_list);
        all_shortcuts = getShortcuts();
        aalv.setAdapter(shortcuts_list_adapter);
        return tr;
    }



    static String[] int_acts = {// TODO fill with intent actions of settings to permit
            Settings.ACTION_ACCESSIBILITY_SETTINGS, Settings.ACTION_ADD_ACCOUNT, Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS, Settings.ACTION_APPLICATION_SETTINGS, Settings.ACTION_BATTERY_SAVER_SETTINGS,
            Settings.ACTION_BLUETOOTH_SETTINGS, Settings.ACTION_DATE_SETTINGS, Settings.ACTION_DEVICE_INFO_SETTINGS,
            Settings.ACTION_DISPLAY_SETTINGS, Settings.ACTION_HOME_SETTINGS, Settings.ACTION_INPUT_METHOD_SETTINGS,
            Settings.ACTION_MANAGE_ALL_APPLICATIONS_SETTINGS, Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS, Settings.ACTION_PRINT_SETTINGS,Settings.ACTION_PRIVACY_SETTINGS,
            Settings.ACTION_QUICK_LAUNCH_SETTINGS, Settings.ACTION_WIFI_IP_SETTINGS, Settings.ACTION_WIFI_SETTINGS,
            Settings.ACTION_WIRELESS_SETTINGS};
    static String[] int_Display_name = {// TODO supply strings to display for the settings above
            "Accessibility", "Add account", "App details settings", "App development setting", "App settings",
            "Battery saver settings", "Bluetooth settings", "Date settings", "Device info settings", "Display settings",
            "Home settings", "Input method settings", "Manage all apps settings", "Manage apps settings", "Print settings",
            "Privacy settings", "Quick launch settings", "WIFI IP settings", "WIFI settings", "Wireless settings"
    };

    public static List<Shortcuts.A_setting_tag> default_allowed_settings(){
        List<Shortcuts.A_setting_tag> tr = new ArrayList<>();
        for(int i = 0 ; i < int_acts.length; i++){
            tr.add(new Shortcuts.A_setting_tag(int_acts[i],true,int_Display_name[i]));
        }
        return tr;
    }


    private BroadcastReceiver mbr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            logDebug(this,"Settings list: refreshing");
            //Toast.makeText(context,"Settings list: refreshing",Toast.LENGTH_LONG).show();
            all_shortcuts = getShortcuts();
            aalv.setAdapter(shortcuts_list_adapter);
        }
    };
}
