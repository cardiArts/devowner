package com.cardiarts.cardilock.frags;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cardiarts.cardilock.R;
import com.cardiarts.cardilock.grips.AllAppsSatusEditAdapter;

import static com.cardiarts.cardilock.grips.Actions.logDebug;

/**
 * Created by Benjamin Awesome on 8/8/2016.
 */
public class EnableAppsFragment extends Fragment {
    private ListView lv_list;
    private AllAppsSatusEditAdapter ad;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ad = new AllAppsSatusEditAdapter(getActivity());
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mbr,new IntentFilter(refresh_action));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View tr = inflater.inflate(R.layout.activity_cardi_launcher_settings,null);
        lv_list = (ListView) tr.findViewById(R.id.lv_apps_);
        lv_list.setAdapter(ad);
        return tr;
    }

    public static final String refresh_action = "need.to.refresh.now";
    private BroadcastReceiver mbr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            logDebug(this,"AccessList: refreshing");
            //Toast.makeText(context,"AccessList: refreshing",Toast.LENGTH_LONG).show();
            ad = new AllAppsSatusEditAdapter(getActivity());
            lv_list.setAdapter(ad);
        }
    };
}
