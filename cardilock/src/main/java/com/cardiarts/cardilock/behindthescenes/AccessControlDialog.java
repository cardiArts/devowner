package com.cardiarts.cardilock.behindthescenes;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cardiarts.cardilock.R;

import static com.cardiarts.cardilock.grips.Actions.is_ac;
import static com.cardiarts.cardilock.grips.Actions.showShortToast;

/**
 * Created by ben on 14/06/2016.
 */
public class AccessControlDialog extends DialogFragment implements View.OnClickListener {
    private Intent acti_to_start;
    private EditText et_ac;
    private Button btn_go;

    public AccessControlDialog() {
    }

    private void setActivityToStart(Intent activity_to_start){
        this.acti_to_start = activity_to_start;
    }

    public static void proceed(Activity d,Intent a){
        AccessControlDialog b = new AccessControlDialog();
        b.setActivityToStart(a);
        b.show(d.getFragmentManager(),"accessing");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View tr = inflater.inflate(R.layout.passsword_dialog,container,false);
        et_ac = (EditText) tr.findViewById(R.id.et_access_code);
        btn_go = (Button) tr.findViewById(R.id.btn_ac);
        btn_go.setOnClickListener(this);
        return tr;
    }

    @Override
    public void onClick(View v) {
        String ev = et_ac.getText().toString();
        if(ev.isEmpty()){
            showShortToast("Enter your access code then press OK");
            return;
        }

        if(is_ac(ev))
            startActivity(acti_to_start);
        else{
            showShortToast("Invalid Access code");
            dismiss();
        }
    }
}
