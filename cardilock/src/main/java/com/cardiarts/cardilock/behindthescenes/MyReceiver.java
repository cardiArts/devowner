package com.cardiarts.cardilock.behindthescenes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;

import static com.cardiarts.cardilock.grips.Actions.logDebug;
import static com.cardiarts.cardilock.grips.Actions.noteAllLaunchables;
import static com.cardiarts.cardilock.grips.OActions.init;

public class MyReceiver extends BroadcastReceiver {
    public MyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        init(context);
        logDebug(this,"Received broadcast with action ["+action+"]");
        //Toast.makeText(context,"Received broadcast with action ["+action+"]",Toast.LENGTH_SHORT).show();

        switch (action){
            case Intent.ACTION_PACKAGE_ADDED:
                logDebug(this,"Received package added broadcast");
                noteAllLaunchables();
                break;

            case Intent.ACTION_PACKAGE_REMOVED:
                logDebug(this,"Received package removed broadcast");
                noteAllLaunchables();
                break;

            case Intent.ACTION_PACKAGE_REPLACED:
                logDebug(this,"Received package replaced broadcast");
                noteAllLaunchables();
                break;

            case Intent.ACTION_PACKAGE_FIRST_LAUNCH:
                logDebug(this,"Received first launch broadcast");
                noteAllLaunchables();
                break;

            case Intent.ACTION_MY_PACKAGE_REPLACED:
                logDebug(this,"Received package replaced broadcast");
                noteAllLaunchables();
                break;

            case Intent.ACTION_BOOT_COMPLETED:
                break;

            case Intent.ACTION_BATTERY_LOW:
                break;

            case TelephonyManager.ACTION_PHONE_STATE_CHANGED:
                break;

            case ConnectivityManager.CONNECTIVITY_ACTION:
                break;
        }
    }

}
