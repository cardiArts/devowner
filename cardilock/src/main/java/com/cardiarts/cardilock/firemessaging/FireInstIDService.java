package com.cardiarts.cardilock.firemessaging;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static com.cardiarts.cardilock.grips.Actions.isConNEcTed;
import static com.cardiarts.cardilock.grips.Actions.logDebug;
import static com.cardiarts.cardilock.grips.Actions.set_FBt_synced;
import static com.cardiarts.cardilock.grips.Actions.storeFBid;
import static com.cardiarts.cardilock.grips.Tasks.ReportFBtokenTask.reportFBToken;

public class FireInstIDService extends FirebaseInstanceIdService {
    public FireInstIDService() {
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        logDebug(this,"Fb ID refreshed as: ["+refreshedToken+"]");
        storeFBid(refreshedToken);
        set_FBt_synced(false);
        if(isConNEcTed())
            reportFBToken();
    }

}
