package com.cardiarts.cardilock.firemessaging;

import android.content.Context;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by ben on 14/06/2016.
 */
public class MyFB_MessageHandler extends Thread {
    private Context base_c;
    private RemoteMessage my_parole;

    private MyFB_MessageHandler(){
    }

    public MyFB_MessageHandler(Context context){
        this.base_c = context.getApplicationContext();
    }

    public MyFB_MessageHandler setFBMessage(RemoteMessage sent_msg){
        this.my_parole = sent_msg;
        return this;
    }

    public void process() throws Exception {
        if(this.my_parole == null)
            throw new Exception("FireBase Message not set");

        Map<String,String> dd = my_parole.getData();
        if(dd != null && dd.size()>0)
            breakItDown(dd);
    }


    private void breakItDown(Map<String,String> data){

    }
    private static final String ALTER_PACKAGE_ACCESSIBILITY = "change_can_access_package";
    private static final String INSTALL_PACKAGE = "install_lackage";
    private static final String OPEN_SPECIFIC_SETTING = "open_setting";
    private static final String UNINSTALL_PACKAGE = "unisntall_package";
    private static final String CHANGE_ACCESS_PASSWORD = "change_m_password";
    private static final String CHANGE_KEYGUARD_ENABLED = "change_can_access_package";
    private static final String SET_PACKAGE_CAN_LOCKTASK = "change_can_access_package";
    private static final String REPORT_LOCATION = "change_can_access_package";
    private static final String SET_SPECIFIC_SETTING_ACCESIBILITY = "change_can_access_package";
}
