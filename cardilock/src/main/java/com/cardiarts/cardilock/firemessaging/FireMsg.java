package com.cardiarts.cardilock.firemessaging;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Iterator;
import java.util.Map;

import static com.cardiarts.cardilock.grips.Actions.logDebug;

public class FireMsg extends FirebaseMessagingService {
    public FireMsg() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        logDebug(this,"Firebase message came in");
        RemoteMessage.Notification recevied_notification = remoteMessage.getNotification();
        Map<String,String> data = remoteMessage.getData();

        logDebug(this,"Delivered message "+(recevied_notification == null ? "does not contain ":"contains ")+"notification paroles");
        logDebug(this,"Delivered message "+(data == null ? "does not contain ":"contains ")+"data paroles");

        if(recevied_notification != null)
            logDebug(this,"Received notification is : "+recevied_notification.toString());

        if(data != null){
            Iterator<String> dks = data.keySet().iterator();
            String data_info = "";
            while(dks.hasNext()){
                String nk = dks.next();
                data_info+=nk+"="+data.get(dks);
            }
            logDebug(this,"Data sent is: ["+data_info+"]");
        }

        try {
            new MyFB_MessageHandler(this).setFBMessage(remoteMessage).process();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
