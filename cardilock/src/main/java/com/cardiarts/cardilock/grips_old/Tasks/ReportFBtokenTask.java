package com.cardiarts.cardilock.grips_old.grips.Tasks;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import static com.cardiarts.cardilock.grips.Actions.getDeviceImei;
import static com.cardiarts.cardilock.grips.Actions.getDeviceMA;
import static com.cardiarts.cardilock.grips.Actions.getDeviceNameandModel;
import static com.cardiarts.cardilock.grips.Actions.getFBid;

/**
 * Created by ben on 14/06/2016.
 */
public class ReportFBtokenTask extends AsyncTask {
    private static boolean running;

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        running = false;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        running = true;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        //report the following to the server
        /*
        imei
        brand
        mac address
        token
         */
        try {
            JSONObject ts = new JSONObject();
            ts.put("device_imei",getDeviceImei());
            ts.put("device_name",getDeviceNameandModel());
            ts.put("device_ma",getDeviceMA());
            ts.put("FB_token",getFBid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void reportFBToken(){
        if(!running){
            new ReportFBtokenTask().execute();
        }
    }

    public ReportFBtokenTask() {
    }
}
