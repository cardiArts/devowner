package com.cardiarts.cardilock.grips_old.grips;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.provider.Settings;

import java.lang.reflect.Method;

import static com.cardiarts.cardilock.grips.Actions.logDebug;

/**
 * Created by ben on 11/06/2016.
 */
public class OActions {
    private static DevicePolicyManager dpm;
    private static ComponentName device_owner_component;
    public static Context refCon;
    private static Object service;
    private static Class<?> statusbarManager;
    private static Method collapse;

    public static void init(Context caller){
        if(refCon != null)
            return;

        refCon = caller.getApplicationContext();
        new CL_DBHelper(refCon);
        device_owner_component = new ComponentName(refCon,DeviceOwnerReceiver.class);
        initSBP();
    }

    public static ComponentName get_dev_admin_comp(){
        return device_owner_component;
    }

    public static void methodname(){

    }

    public static void setEncryptStorage(){
        dpm.setStorageEncryption(device_owner_component,true);
    }

    public static void set_adb_state(boolean state){
        dpm.setGlobalSetting(device_owner_component,Settings.Global.ADB_ENABLED, state?"1":"0");
    }

    public static void set_screen_capturable(boolean v){
        dpm.setScreenCaptureDisabled(device_owner_component,v);
    }

    public static void setProfileName(String name){
        dpm.setProfileName(device_owner_component,name);
    }

    public static void enable_settings_access(){

    }

    public static void collapseStatusBar(){
        try {
            logDebug("attempting to collapse status bar");
            collapse.invoke(service);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void expandStatusBar(){
        try {
            logDebug("attempting to expand status bar");
            if(statusbarManager == null)
                return;

            Method tf = statusbarManager.getMethod("expand");
            tf.setAccessible(true);
            tf.invoke(service);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void initSBP(){
        try {
            service = refCon.getSystemService("statusbar");
            statusbarManager = Class.forName("android.app.StatusBarManager");
            collapse = statusbarManager.getMethod("collapse");
            collapse.setAccessible(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static boolean tr = false;
    public static boolean sr = false;
    public static void lockStatusBar(){
        if(tr)
            return;

        sr = true;
        new T().start();
    }

    private static class T extends Thread{
        long wait = 1l;
        @Override
        public void run() {
            tr = true;
            do{
                collapseStatusBar();
                long now = System.currentTimeMillis();
                long when = now+wait;
                //while(System.currentTimeMillis() < when){}
            }while(true && sr);
            tr = false;
        }
    }


    /*
    Secure setting would include:

     */

    /*
    Global setting would include

     */


}
