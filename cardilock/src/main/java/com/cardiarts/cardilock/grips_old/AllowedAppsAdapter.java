package com.cardiarts.cardilock.grips_old.grips;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.LauncherActivityInfo;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cardiarts.cardilock.R;

import java.util.ArrayList;
import java.util.List;

import static com.cardiarts.cardilock.behindthescenes.AccessControlDialog.proceed;
import static com.cardiarts.cardilock.grips.Actions.getLayoutInflater;

/**
 * Created by ben on 12/06/2016.
 */
public class AllowedAppsAdapter extends BaseAdapter{

    private final Activity act;
    private List<LauncherActivityInfo> info_s = new ArrayList<>();

    public AllowedAppsAdapter(Activity tu, List<LauncherActivityInfo> app_lauchers_infor)
    {
        this.act = tu;
        this.info_s = app_lauchers_infor;
    }


    @Override
    public int getCount() {
        return info_s.size();
    }

    @Override
    public LauncherActivityInfo getItem(int position) {
        return info_s.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View tr = getLayoutInflater().inflate(R.layout.app_rep,parent,false);
        TextView l = (TextView) tr.findViewById(R.id.app_tv_label);
        ImageView i = (ImageView) tr.findViewById(R.id.app_iv_icon);
        final LauncherActivityInfo cur = getItem(position);
        l.setText(cur.getLabel());
        i.setImageDrawable(cur.getIcon(0));
        tr.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent n = //new Intent(act,));
                        Intent.makeMainActivity(cur.getComponentName());
                        n.setFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY|Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
                        |Intent.FLAG_ACTIVITY_NEW_TASK);
                        if(test_password)
                            proceed(act,n);
                        else
                            act.startActivity(n);
                    }
                }
        );
        return tr;
    }

    private boolean test_password = false;
}
