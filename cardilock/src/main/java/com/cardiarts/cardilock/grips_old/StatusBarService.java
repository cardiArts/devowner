package com.cardiarts.cardilock.grips_old.grips;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.WindowManager;

public class StatusBarService extends Service {
    private customViewGroup view;
    private boolean hv;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        overlayparole();
    }

    private void overlayparole() {
        WindowManager.LayoutParams mwmklp =
                new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        //WindowManager.LayoutParams.WRAP_CONTENT,
                        (int)
                                (24 * (1 * getResources()
                                        .getDisplayMetrics().scaledDensity)),

                        //WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                        WindowManager.LayoutParams.TYPE_SYSTEM_ERROR
                        //,WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN
                        //,WindowManager.LayoutParams.FLAG_FULLSCREEN
                        ,WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|

                        // this is to enable the notification to recieve touch events
                        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                        // Draws over status bar
                        WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN

                        //WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        ,//| WindowManager.LayoutParams.FLAG_FULLSCREEN
                        //| WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        PixelFormat.TRANSLUCENT
                );
        mwmklp.gravity = Gravity.TOP;
        //mwmklp.format = PixelFormat.TRANSPARENT;
        final WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        //tp = getLayoutInflater().inflate(R.layout.activity_face, null);
        //tp.onTouchEvent()

        //getSupportActionBar().
        //      setBackgroundDrawable(new ColorDrawable(Color.parseColor("#330000ff")));

        view = new customViewGroup(this);
        //view.setBackgroundColor(Color.RED);
        hv = true;

        wm.addView(view, mwmklp);
        //startActivity(new Intent(this, Face.class));*/


        //OActions.lockStatusBar();

/*
            WindowManager manager = ((WindowManager) getApplicationContext()
                    .getSystemService(Context.WINDOW_SERVICE));

            WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
            localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
            localLayoutParams.gravity = Gravity.TOP;
            localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|

                    // this is to enable the notification to recieve touch events
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                    // Draws over status bar
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

            localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            localLayoutParams.height = (int) (50 * getResources()
                    .getDisplayMetrics().scaledDensity);
            localLayoutParams.format = PixelFormat.TRANSPARENT;

            view = new customViewGroup(this);

            manager.addView(view, localLayoutParams);*/
    }

    @Override
    public void onDestroy() {
        WindowManager manager = ((WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE));
        if(view != null && hv) {
            manager.removeViewImmediate(view);
            hv = false;
        }
        super.onDestroy();
    }
}
