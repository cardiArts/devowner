/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.android.deviceowner;

public final class R {
    public static final class attr {
        /** <p>Must be a color value, in the form of "<code>#<i>rgb</i></code>", "<code>#<i>argb</i></code>",
"<code>#<i>rrggbb</i></code>", or "<code>#<i>aarrggbb</i></code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardBackgroundColor=0x7f010000;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardCornerRadius=0x7f010001;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardElevation=0x7f010002;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardMaxElevation=0x7f010003;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardPreventCornerOverlap=0x7f010005;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int cardUseCompatPadding=0x7f010004;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int contentPadding=0x7f010006;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int contentPaddingBottom=0x7f01000a;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int contentPaddingLeft=0x7f010007;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int contentPaddingRight=0x7f010008;
        /** <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int contentPaddingTop=0x7f010009;
    }
    public static final class color {
        public static final int cardview_dark_background=0x7f070000;
        public static final int cardview_light_background=0x7f070001;
        public static final int cardview_shadow_end_color=0x7f070002;
        public static final int cardview_shadow_start_color=0x7f070003;
    }
    public static final class dimen {
        public static final int cardview_compat_inset_shadow=0x7f050002;
        public static final int cardview_default_elevation=0x7f050003;
        public static final int cardview_default_radius=0x7f050004;
        public static final int horizontal_page_margin=0x7f050000;
        public static final int margin_huge=0x7f050005;
        public static final int margin_large=0x7f050006;
        public static final int margin_medium=0x7f050007;
        public static final int margin_small=0x7f050008;
        public static final int margin_tiny=0x7f050009;
        public static final int vertical_page_margin=0x7f050001;
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
        public static final int tile=0x7f020001;
    }
    public static final class id {
        public static final int available_launchers=0x7f090003;
        public static final int container=0x7f090000;
        public static final int set_preferred_launcher=0x7f090004;
        public static final int switch_auto_time=0x7f090001;
        public static final int switch_auto_time_zone=0x7f090002;
    }
    public static final class layout {
        public static final int activity_main_real=0x7f030000;
        public static final int fragment_device_owner=0x7f030001;
        public static final int fragment_instruction=0x7f030002;
    }
    public static final class string {
        public static final int app_name=0x7f080000;
        public static final int clear_preferred=0x7f080001;
        public static final int intro_message=0x7f080002;
        public static final int label_auto_time=0x7f080003;
        public static final int label_auto_time_zone=0x7f080004;
        public static final int label_global_settings=0x7f080005;
        public static final int label_launcher=0x7f080006;
        public static final int profile_name=0x7f080007;
        public static final int set_as_preferred=0x7f080008;
        public static final int set_up_instruction=0x7f080009;
    }
    public static final class style {
        public static final int AppTheme=0x7f060002;
        public static final int CardView=0x7f060003;
        public static final int CardView_Dark=0x7f060004;
        public static final int CardView_Light=0x7f060005;
        public static final int Theme_Base=0x7f060001;
        public static final int Theme_Sample=0x7f060006;
        public static final int Widget=0x7f060007;
        public static final int Widget_SampleMessage=0x7f060000;
        public static final int Widget_SampleMessageTile=0x7f060008;
    }
    public static final class xml {
        public static final int device_owner_receiver=0x7f040000;
    }
    public static final class styleable {
        /** Attributes that can be used with a CardView.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #CardView_cardBackgroundColor com.example.android.deviceowner:cardBackgroundColor}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_cardCornerRadius com.example.android.deviceowner:cardCornerRadius}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_cardElevation com.example.android.deviceowner:cardElevation}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_cardMaxElevation com.example.android.deviceowner:cardMaxElevation}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_cardPreventCornerOverlap com.example.android.deviceowner:cardPreventCornerOverlap}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_cardUseCompatPadding com.example.android.deviceowner:cardUseCompatPadding}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_contentPadding com.example.android.deviceowner:contentPadding}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_contentPaddingBottom com.example.android.deviceowner:contentPaddingBottom}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_contentPaddingLeft com.example.android.deviceowner:contentPaddingLeft}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_contentPaddingRight com.example.android.deviceowner:contentPaddingRight}</code></td><td></td></tr>
           <tr><td><code>{@link #CardView_contentPaddingTop com.example.android.deviceowner:contentPaddingTop}</code></td><td></td></tr>
           </table>
           @see #CardView_cardBackgroundColor
           @see #CardView_cardCornerRadius
           @see #CardView_cardElevation
           @see #CardView_cardMaxElevation
           @see #CardView_cardPreventCornerOverlap
           @see #CardView_cardUseCompatPadding
           @see #CardView_contentPadding
           @see #CardView_contentPaddingBottom
           @see #CardView_contentPaddingLeft
           @see #CardView_contentPaddingRight
           @see #CardView_contentPaddingTop
         */
        public static final int[] CardView = {
            0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003,
            0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007,
            0x7f010008, 0x7f010009, 0x7f01000a
        };
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#cardBackgroundColor}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a color value, in the form of "<code>#<i>rgb</i></code>", "<code>#<i>argb</i></code>",
"<code>#<i>rrggbb</i></code>", or "<code>#<i>aarrggbb</i></code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:cardBackgroundColor
        */
        public static final int CardView_cardBackgroundColor = 0;
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#cardCornerRadius}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:cardCornerRadius
        */
        public static final int CardView_cardCornerRadius = 1;
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#cardElevation}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:cardElevation
        */
        public static final int CardView_cardElevation = 2;
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#cardMaxElevation}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:cardMaxElevation
        */
        public static final int CardView_cardMaxElevation = 3;
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#cardPreventCornerOverlap}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:cardPreventCornerOverlap
        */
        public static final int CardView_cardPreventCornerOverlap = 5;
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#cardUseCompatPadding}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:cardUseCompatPadding
        */
        public static final int CardView_cardUseCompatPadding = 4;
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#contentPadding}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:contentPadding
        */
        public static final int CardView_contentPadding = 6;
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#contentPaddingBottom}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:contentPaddingBottom
        */
        public static final int CardView_contentPaddingBottom = 10;
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#contentPaddingLeft}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:contentPaddingLeft
        */
        public static final int CardView_contentPaddingLeft = 7;
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#contentPaddingRight}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:contentPaddingRight
        */
        public static final int CardView_contentPaddingRight = 8;
        /**
          <p>This symbol is the offset where the {@link com.example.android.deviceowner.R.attr#contentPaddingTop}
          attribute's value can be found in the {@link #CardView} array.


          <p>Must be a dimension value, which is a floating point number appended with a unit such as "<code>14.5sp</code>".
Available units are: px (pixels), dp (density-independent pixels), sp (scaled pixels based on preferred font size),
in (inches), mm (millimeters).
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.example.android.deviceowner:contentPaddingTop
        */
        public static final int CardView_contentPaddingTop = 9;
    };
}
